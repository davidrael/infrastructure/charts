# Introduction 
This repository contains Helm charts. The charts are useful for deploying applications into Kubernetes clusters.

# Usage
The intent of everything here is to use ArgoCD to deploy applications and keep them up to date with the latest versions as they move forward.

These charts have values files that set parameters for the various charts, especially for given environments. Applications are being set up in ArgoCD via Terraform. These applications watch this repository to deploy updated charts.

# Chart Basics
Each chart in this repository is created as a subdirectory of the root. A Chart.yaml file is required in the directory for the chart to specify the meta-information about the chart. Values files also exist in this directory. A file named values.yaml is automatically used in every deployment and other values files can also be explicitly used. ArgoCD applications include the values specified in a values file for a given environment.

Templated Kubernetes manifests go into the subdirectory named templates. It's these templates that determine the resources that get created in the cluster when the chart is installed.
